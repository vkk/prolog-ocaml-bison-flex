% Helper Predicates
%  1.  imageRow(+Image, -ImageRow) 
%  2.  image(+Image, -ImageCopy) 
%  3.  printRows(+Image)
%  4.  zeroRow(+ImageRow)
%  5.  negMask(+Image1Row, +DiffImRow, -I1MaskRow)
%  6.  posMask(+Image2Row, +DiffImRow, -I2MaskRow)
%  7.  setColumns(+ImageMask, -Index, -Size)
%  8.  getColumns(+ImageMask, +Index, +Size, -X)
%  9.  getI(+ImageMask, +Index, -I)
%  10. getJ(+ImageMask, -J)


% SDE1 Predicates	
%  1.  diffE1(+E1, +E2, -D)		
%  2.  diffRow(+IR1, +IR2, -DR)	
%  3.  diffIm(+Image1, +Image2, -Diff)	
%  4.  printImage(+Image)	
%  5.  isDiff(+Image1, +Image2)
%  6.  rowmask(+Image1Row, +Image2Row, +DiffImRow, -I1MaskRow, -I2MaskRow) 
%  7.  mask(+Image1, +Image2, +DiffIm, -I1Mask, -I2Mask)
%  8.  fnzRowE1(+Mask, -Index)
%  9.  firstNonZero(+MaskImage, -RowIndex, -ColIndex)
%  10. motion(+Image1, +Image2, -Moti, -Motj)


%   SDE1 suggests the reader create 'imageRow' and 'image' predicates, 
%   but I have not used them in my main solution--just in testing with 
%   sample input. In the 'sampleImagesGoals.pro' file provided, 'imageRow'
%   and 'image' are defined for static procedures, so I'm assuming that
%   is where these predicates belong. If not, I've listed and commented
%   them out below just for reference.  
%
%   imageRow(_,_).
%   imageRow([H|_],H). 
%
%   image(_,_).
%   image([H|T],[H|Image]):- image(T,Image),!.


diffE1(0,0,0).
diffE1(E1,E2,D):- D is E2-E1.

diffRow([],[],[]).
diffRow([H1|R1],[H2|R2],[D|R]):-
	diffE1(H1,H2,D),
        diffRow(R1,R2,R),!.

diffIm([],[],[]).
diffIm([R1|Image1],[R2|Image2],[R|Diff]):-
	diffRow(R1,R2,R),
	diffIm(Image1,Image2,Diff),!.

printRow([]).
printRow([H|T]):- write(H), tab(1), printRow(T).

printRows([]).
printRows([H|T]):- nl, printRow(H), nl, printRows(T). 

printImage([]).
printImage(I):- printRows(I), nl,!.

zeroRow([]).
zeroRow([H|T]):- H == 0, zeroRow(T). 

zeroImage([]).
zeroImage([H|T]):- zeroRow(H), zeroImage(T).

isDiff([],[]).
isDiff(Image1,Image2):- 
	diffIm(Image1,Image2,Diff), 
	not(zeroImage(Diff)),!.

negMask([],[],[]).
negMask([H|Image1Row], [D|DiffImRow], [M|I1MaskRow]):-
	D >= 0, M is 0, negMask(Image1Row,DiffImRow,I1MaskRow);
	D < 0, M is H, negMask(Image1Row,DiffImRow,I1MaskRow).

posMask([],[],[]).
posMask([H|Image2Row], [D|DiffImRow], [M|I2MaskRow]):-
	D =< 0, M is 0, posMask(Image2Row,DiffImRow,I2MaskRow);
	D > 0, M is H, posMask(Image2Row,DiffImRow,I2MaskRow).

rowmask([],[],[],[],[]).
rowmask(Image1Row,Image2Row,DiffImRow,I1MaskRow,I2MaskRow):-
	negMask(Image1Row,DiffImRow,I1MaskRow),
	posMask(Image2Row,DiffImRow,I2MaskRow),!.
	
mask([],[],[],[],[]).
mask([H1|Image1],[H2|Image2],[D|DiffIm],[M1|I1Mask],[M2|I2Mask]):-
	rowmask(H1,H2,D,M1,M2),
	mask(Image1,Image2,DiffIm,I1Mask,I2Mask),!.

setColumns([],[],0).
setColumns([_|Mask],[I|Index],Size):-
	setColumns(Mask,Index,S),
	Size is S + 1, I is Size,!.
	
getColumns([],[],0,0).
getColumns([M|Mask],[I|Index],Size,X):-
	not(M == 0), X is Size - I + 1;
	getColumns(Mask,Index,Size,X).

fnzRowE1([],0).
fnzRowE1(Mask,X):-
	setColumns(Mask,Index,Size),
	getColumns(Mask,Index,Size,X),!.

getI([],0,0).
getI([M|Mask],Index,I):-
	zeroRow(M), getI(Mask,Index,I + 1);
	Index is I.	

getJ([],0).
getJ([M|Mask],J):- 
	zeroRow(M), getJ(Mask,J);
	fnzRowE1(M,J).

firstNonZero([],0,0).
firstNonZero([M|Mask],I,J):- getI([M|Mask],I,1), getJ([M|Mask],J),!. 

motion([],[],0,0).
motion(Image1,Image2,Moti,Motj):-
	diffIm(Image1,Image2,Diff),
	zeroImage(Diff),
	nl,write('***** No Motion in This Case *****'),nl,!;
	diffIm(Image1,Image2,Diff),
	mask(Image1,Image2,Diff,Mask1,Mask2),
	firstNonZero(Mask1,R1,C1),
	firstNonZero(Mask2,R2,C2),
	Moti is R2 - R1, Motj is C2 - C1.

