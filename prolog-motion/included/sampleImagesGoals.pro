/* simple example data for SDE1 */

image(i1Sample,[   
[2,2,2,2,2,2,2,2,2],
[2,8,8,8,2,2,2,2,2],
[2,8,8,8,2,2,2,2,2],
[2,8,8,8,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2]]).

image(i2Sample,[ 
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2,2],
[2,2,2,2,2,8,8,8,2],
[2,2,2,2,2,8,8,8,2],
[2,2,2,2,2,8,8,8,2],
[2,2,2,2,2,2,2,2,2]]).

/* notice change in image dimensions and object (target) shape */

image(i1Cross, [
[2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,4,2],
[2,2,2,2,2,4,4,4],
[2,2,2,2,2,2,4,2]]).

image(i2Cross, [
[2,2,2,2,2,2,2,2],
[2,2,2,4,2,2,2,2],
[2,2,4,4,4,2,2,2],
[2,2,2,4,2,2,2,2],
[2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2],
[2,2,2,2,2,2,2,2]]).


image(i1Tri, [ 
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,9,5,5,5,5,5,5],
[5,5,9,9,5,5,5,5,5,5],
[5,9,9,9,5,5,5,5,5,5],
[9,9,9,9,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5]]).

image(i2Tri, [
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,5,5],
[5,5,5,5,5,5,5,5,9,5],
[5,5,5,5,5,5,5,9,9,5],
[5,5,5,5,5,5,9,9,9,5],
[5,5,5,5,5,9,9,9,9,5],
[5,5,5,5,5,5,5,5,5,5]]).


imageRow(i1Sample,[2,8,8,8,2,2,2,2,2]).

imageRow(i2Sample,[2,2,2,2,2,8,8,8,2]).

/* here's some goals represented as Prolog rules --
   others are shown in the SDE1 assignment pdf */

goal1Sample(Moti,Motj) :- image(i1Sample,I1),image(i2Sample,I2),motion(I1,I2,Moti,Motj). /* 'Sample' images */

goal2Sample(Moti,Motj) :- image(i1Sample,I1),image(i1Sample,I2),motion(I1,I2,Moti,Motj). /* no  motion */

goal3Sample(Moti,Motj) :- image(i1Cross,I1),image(i2Cross,I2),motion(I1,I2,Moti,Motj). /* 'Cross' Images */

goal4Sample(Moti,Motj) :- image(i1Tri,I1),image(i2Tri,I2),motion(I1,I2,Moti,Motj). /* 'Tri' images */

goal5Sample(Moti,Motj) :- image(i2Tri,I1),image(i1Tri,I2),motion(I1,I2,Moti,Motj). /* reversed 'Tri' */


