tverkai-SDE1 contents:

    1. readme.txt - file descriptions and readme text file 
    2. motion.pro - prolog source code; 10 base predicates written to implement 
	an image tracking algorithm using declarative programming; 10 helper
	predicates designed to practice incremental development.   
    3. motion.log - sample operations of each predicate including 
	multiple 'no motion' cases.

