%{
#include <stdlib.h>
#include <stdbool.h>
extern int yylex();
extern int yyerror(char *s);
bool error = false;
bool cc_list = false;
bool needs_list = false;
bool re_entry = false;
bool re_list = false;
bool un_list = false; 
%}

%token LBRACK 
%token RBRACK
%token COMMA
%token DAY 
%token TIME
%token SECTION
%token COURSE
%token STUDENT

%% 

expression: needs { needs_list = true; 
		cc_list = error = re_entry = re_list = un_list = !needs_list; } 
	| unavailable_list { un_list = true; 
		cc_list = error = needs_list = re_entry = re_list = !un_list; } 
	| LBRACK course_capabilities_list RBRACK { cc_list = true; 
		error = needs_list = re_entry = re_list = un_list = !cc_list; } 
	| LBRACK resources_entry RBRACK { re_entry = true;
		cc_list = error = needs_list = re_list = un_list = !re_entry; } 
	| LBRACK resources_list RBRACK { re_list= true;
		cc_list = error = needs_list = re_entry = un_list = !re_list; } 
	| error { error = true;
		cc_list = needs_list = re_entry = re_list = un_list = !error; } 
	;

needs: LBRACK needs RBRACK
	| course_list COMMA needs
	| course_list
	;

course_list: LBRACK COURSE COMMA SECTION COMMA DAY COMMA start_end RBRACK 
	;

start_end: SECTION COMMA SECTION
	| SECTION COMMA TIME
	| TIME COMMA TIME
	| TIME COMMA SECTION
	;

resources_list: LBRACK resources_entry RBRACK COMMA resources_list
	| LBRACK resources_entry RBRACK 
	;

resources_entry: student course_capabilities_list RBRACK COMMA unavailable_list 
	;

student: STUDENT COMMA LBRACK
	;

course_capabilities_list: COURSE COMMA course_capabilities_list 
	| COURSE 
	;

unavailable_list: LBRACK  unavailable_list RBRACK
	| unavailable COMMA unavailable_list
	| unavailable 
	| LBRACK RBRACK 
	;

unavailable: LBRACK DAY COMMA TIME COMMA TIME RBRACK
	;

%%
