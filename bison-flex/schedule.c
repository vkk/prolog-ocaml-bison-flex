#include "schedule.tab.c"
#include "lex.yy.c"
#include "yyerror.c"
//#define YYERROR_VERBOSE

int main() {

   yyparse();

   if(needs_list) printf("found valid 'needs' list \n");
   else if(un_list) printf("found valid 'unavailable' list \n");
   else if(cc_list) printf("found valid 'course_capabilities' list \n");
   else if(re_entry) printf("found valid 'resources_entry' list \n"); 
   else if(re_list) printf("found valid 'resources' list \n");
   else return -1;

   return 0;
}
