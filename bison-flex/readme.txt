   (1) readme.txt: file descriptions and readme text file   

   (2) schedule.in: flex input file

   (3) schedule.y: bison file

   (4) schedule.c: overall sde C source file

   (5) schedule.log: a log of 6 sample uses of the resulting 
   executable for each element required to be recognized in 
   SDE 1 documentation, Section 4.1.

   (6) buildit: bash script using flex and bison to build
   the executable file, schedule.

   (7) yyerror.c: simple error function  
